import 'package:flutter/material.dart';
import 'package:grosss/tahun19.dart';
import 'package:grosss/tahunan.dart';

class DataPage extends StatefulWidget {
  @override
  _DataPageState createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data RTH'),
      ),
      backgroundColor: Color.fromARGB(
          255, 217, 247, 236), // Mengganti warna background menjadi kalem
      body: ListView(
        children: [
          SizedBox(height: 16.0), // Tambahkan jarak sebesar 16.0
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 20.0),
                Card(
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/problem-solving.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text(
                      'Data RTH',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Text(
                    'Idealnya sebuah kota harus memiliki ruang terbuka hijau seluas 30 persen dari total luas kota',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Text(
                    'Dalam jangka waktu 3 tahun terdapat beberapa kelurahan yang berhasil menambah lokasi RTH',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(height: 20.0),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DataTahunanPage(),
                      ),
                    );
                  },
                  child: Text(
                    'Data Tahunan RTH',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
