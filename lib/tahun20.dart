import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:csv/csv.dart';

class Tahun2020Page extends StatefulWidget {
  @override
  _Tahun2020PageState createState() => _Tahun2020PageState();
}

class _Tahun2020PageState extends State<Tahun2020Page> {
  List<List<dynamic>> data = [];

  @override
  void initState() {
    super.initState();
    fetchDataFromCSV();
  }

  Future<void> fetchDataFromCSV() async {
    final String csvData =
        await rootBundle.loadString('assets/csv/data2020.csv');
    List<List<dynamic>> csvResult = CsvToListConverter().convert(csvData);
    setState(() {
      data = csvResult;
    });
  }

  List<DataColumn> _buildColumns() {
    return data.isNotEmpty
        ? List<DataColumn>.generate(
            data[0].length,
            (int columnIndex) => DataColumn(
              label: Text(data[0][columnIndex].toString()),
              numeric: true,
            ),
          )
        : [];
  }

  List<DataRow> _buildRows() {
    return List<DataRow>.generate(
      data.length - 1,
      (int rowIndex) => DataRow(
        cells: List<DataCell>.generate(
          data[rowIndex + 1].length,
          (int cellIndex) => DataCell(
            Text(data[rowIndex + 1][cellIndex].toString()),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tahun 2020'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: _buildColumns(),
          rows: _buildRows(),
        ),
      ),
    );
  }
}
