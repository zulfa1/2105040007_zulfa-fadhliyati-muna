import 'package:flutter/material.dart';
import 'package:grosss/logoutconfirmation.dart';
import 'package:grosss/tentangkec.dart';
import 'profildinas.dart';
import 'profilrth.dart';
import 'tentangkec.dart';
import 'data.dart';
import 'update.dart';
import 'profil.dart';
import 'logoutconfirmation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Your App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      backgroundColor: Color.fromARGB(
          255, 217, 247, 236), // Mengganti warna background menjadi kalem
      body: ListView(
        children: [
          SizedBox(height: 25.0), // Tambahkan jarak sebesar 16.0
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 25.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 160.0,
                  height: 160.0,
                ),
                SizedBox(height: 25.0),
                Text(
                  "Menu",
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15.0), // SizedBox pertama sebelum menu pertama
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: 32.0), // Mengatur padding horizontal sebesar 24.0
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilDinasPage()),
                );
              },
              child: Card(
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/save.png',
                    width: 24.0,
                    height: 24.0,
                  ),
                  title: Text('Profil Dinas'),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: 32.0), // Mengatur padding horizontal sebesar 24.0
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilRthPage()),
                );
                // Aksi ketika item Profil RTH di-tap
                // Tambahkan logika aksi yang diinginkan di sini
              },
              child: Card(
                child: Container(
                  width: 80.0, // Atur lebar sesuai kebutuhan Anda
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/planting.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text('Profil RTH'),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 32.0),
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => TentangPage()),
                );
                // Aksi ketika item Profil RTH di-tap
                // Tambahkan logika aksi yang diinginkan di sini
              }, // Mengatur padding horizontal sebesar 24.0
              child: Card(
                child: ListTile(
                  leading: Image.asset(
                    'assets/images/research.png',
                    width: 24.0,
                    height: 24.0,
                  ),
                  title: Text('Tentang Kec Batununggal'),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: 32.0), // Mengatur padding horizontal sebesar 15.0
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => DataPage()),
                );
                // Aksi ketika item Data RTH di-tap
                // Tambahkan logika aksi yang diinginkan di sini
              },
              child: Card(
                child: Container(
                  width: 80.0, // Atur lebar sesuai kebutuhan Anda
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/problem-solving.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text('Data RTH'),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.symmetric(
                horizontal: 32.0), // Mengatur padding horizontal sebesar 15.0
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UpdatePage()),
                );
                // Aksi ketika item Update RTH di-tap
                // Tambahkan logika aksi yang diinginkan di sini
              },
              child: Card(
                child: Container(
                  width: 80.0, // Atur lebar sesuai kebutuhan Anda
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/update.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text('Update RTH'),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 15.0),
          // Tambahkan kode lainnya sesuai kebutuhan Anda
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        color: Color.fromARGB(255, 92, 167, 150),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton.icon(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfilePage()),
                );
                // Aksi ketika tombol profile di-tap
              },
              icon: Icon(
                Icons.person,
                color: Colors.black,
              ),
              label: Text(
                'Profile',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                minimumSize: Size(
                    100.0, 36.0), // Ubah ukuran tombol sesuai kebutuhan Anda
              ),
            ),
            ElevatedButton.icon(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LogoutConfirmationPage()),
                );
                // Aksi ketika tombol logout di-tap
              },
              icon: Icon(
                Icons.logout,
                color: Colors.black,
              ),
              label: Text(
                'Log Out',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
              style: ElevatedButton.styleFrom(
                primary: Colors.white,
                minimumSize: Size(
                    100.0, 36.0), // Ubah ukuran tombol sesuai kebutuhan Anda
              ),
            ),
          ],
        ),
      ),
    );
  }
}
