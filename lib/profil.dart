import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 30.0),
            Text(
              'Profile',
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 30.0),
            CircleAvatar(
              radius: 80.0,
              backgroundImage: AssetImage(
                  'assets/images/avatar.png'), // Ganti dengan path atau URL gambar avatar
            ),
            SizedBox(height: 20.0),
            Card(
              child: ListTile(
                leading: Icon(Icons.person),
                title: Text(
                  'Wulandari',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Card(
              child: ListTile(
                leading: Icon(Icons.email),
                title: Text(
                  'wulandari@example.com',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
            SizedBox(height: 15.0),
            Card(
              child: ListTile(
                leading: Icon(Icons.phone),
                title: Text(
                  '085xxxxxxxx',
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
