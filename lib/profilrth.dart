import 'package:flutter/material.dart';

class ProfilRthPage extends StatefulWidget {
  @override
  _ProfilRthPageState createState() => _ProfilRthPageState();
}

class _ProfilRthPageState extends State<ProfilRthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profil RTH'),
      ),
      backgroundColor: Color.fromARGB(
          255, 217, 247, 236), // Mengganti warna background menjadi kalem
      body: ListView(
        children: [
          SizedBox(height: 16.0), // Tambahkan jarak sebesar 16.0
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 20.0),
                Card(
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/planting.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text(
                      'Profil RTH',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Text(
                    'Berdasarkan Undang-Undang No. 26 Tahun 2007 tentang Penataan Ruang, "Ruang Terbuka Hijau adalah area memanjang/jalur dan/atau mengelompok, yang penggunaannya lebih bersifat terbuka, tempat tumbuh tanaman, baik yang tumbuh secara alamiah maupun yang sengaja ditanam"',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Apa itu Ruang Terbuka Hijau?\n\n',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text:
                              'Habitat liar/alami, kawasan lindung, pertanian kota, pertamanan kota, lapangan olah raga, pemakaman, yang berlokasi pada lahan-lahan publik atau lahan yang dimiliki oleh pemerintah, yang berlokasi pada lahan-lahan milik privat',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
