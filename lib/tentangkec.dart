import 'package:flutter/material.dart';

class TentangPage extends StatefulWidget {
  @override
  _TentangPageState createState() => _TentangPageState();
}

class _TentangPageState extends State<TentangPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tentang Kecamatan'),
      ),
      backgroundColor: Color.fromARGB(
          255, 217, 247, 236), // Mengganti warna background menjadi kalem
      body: ListView(
        children: [
          SizedBox(height: 16.0), // Tambahkan jarak sebesar 16.0
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 20.0),
                Card(
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/research.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text(
                      'Tentang Kec Batununggal',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: Text(
                    'Batununggal adalah sebuah kecamatan di Kota Bandung, Provinsi Jawa Barat, Indonesia. Luas kecamatan Batununggal 526,94 Ha berada di ±755 mdpl. Kecamatan ini terdiri atas 8 kelurahan yaitu, Kelurahan Gumuruh, Binong, Kebon gedang, Maleer, Cibangkong, Samoja, Kacapiring dan Kebonwaru.',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
