import 'package:flutter/material.dart';

class ProfilDinasPage extends StatefulWidget {
  @override
  _ProfilDinasPageState createState() => _ProfilDinasPageState();
}

class _ProfilDinasPageState extends State<ProfilDinasPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profil RTH'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: ListView(
        children: [
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 24.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 20.0),
                SizedBox(
                  width: double.infinity,
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/save.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text(
                        'Profil Dinas',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(16.0),
                  color: Colors.white,
                  child: RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: 'Dinas Lingkungan Hidup\n\n',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                          text:
                              'Salah satu tugas dan fungsi Dinas Lingkungan Hidup adalah Pemeliharaan Ruang Terbuka Hijau (RTH).',
                          style: TextStyle(
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ],
                    ),
                    textAlign: TextAlign.justify,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
