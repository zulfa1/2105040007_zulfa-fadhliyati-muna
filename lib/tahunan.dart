import 'package:flutter/material.dart';
import 'package:grosss/tahun19.dart';
import 'tahun18.dart';
import 'tahun20.dart';

class DataTahunanPage extends StatefulWidget {
  @override
  _DataTahunanPageState createState() => _DataTahunanPageState();
}

class _DataTahunanPageState extends State<DataTahunanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Tahunan'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: ListView(
        children: [
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 15.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 15.0),
                Card(
                  color: Color.fromARGB(
                      255, 92, 167, 150), // Ganti dengan warna yang sesuai
                  child: ListTile(
                    title: Center(
                      child: Text(
                        'Data Tahunan',
                        style: TextStyle(
                          color: Colors
                              .white, // Ganti dengan warna teks yang sesuai
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback to handle when Kelurahan Kebonwaru card is tapped
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Tahun2018Page()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      title: Text('RTH Tahun 2018'),
                    ),
                  ),
                ),
                SizedBox(height: 15.0),
                InkWell(
                  onTap: () {
                    // Callback to handle when Kelurahan Kebonwaru card is tapped
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Tahun2019Page()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      title: Text('RTH Tahun 2019'),
                    ),
                  ),
                ),
                SizedBox(height: 15.0),
                InkWell(
                  onTap: () {
                    // Callback to handle when Kelurahan Kebonwaru card is tapped
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Tahun2020Page()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      title: Text('RTH Tahun 2020'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
