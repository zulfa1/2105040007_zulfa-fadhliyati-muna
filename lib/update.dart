import 'package:flutter/material.dart';
import 'package:grosss/maleer.dart';
import 'Kebonwaru.dart';
import 'gumuruh.dart';
import 'samoja.dart';
import 'kacapiring.dart';
import 'maleer.dart';
import 'binong.dart';
import 'kebongedang.dart';
import 'cibangkong.dart';

class UpdatePage extends StatefulWidget {
  @override
  _UpdatePageState createState() => _UpdatePageState();
}

class _UpdatePageState extends State<UpdatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Update RTH'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: ListView(
        children: [
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  "Green Open Space",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 20.0),
                Image.asset(
                  'assets/images/save.png',
                  width: 150.0,
                  height: 150.0,
                ),
                SizedBox(height: 20.0),
                Card(
                  child: ListTile(
                    leading: Image.asset(
                      'assets/images/update.png',
                      width: 24.0,
                      height: 24.0,
                    ),
                    title: Text('Update RTH'),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Kebonwaru diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => KebonwaruPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Kebonwaru (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 3 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Gumuruh diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => GumuruhPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Gumuruh (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 3 -> 5 lokasi'),
                    ),
                  ),
                ),
                // Tambahkan InkWell untuk setiap Card kelurahan lainnya dengan onTap yang sesuai
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Samoja diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => SamojaPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Samoja (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 1 -> 3 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Kacapiring diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => KacapiringPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Kacapiring (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 1 -> 3 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Maleer diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MaleerPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Maleer (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 2 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Binong diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BinongPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Binong (2018-2020)'),
                      subtitle: Text('Jumlah Lokasi: 3 -> 6 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Kebon Gedang diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => KebonGedangPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Kebon Gedang (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 2 lokasi'),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                InkWell(
                  onTap: () {
                    // Callback untuk menangani saat Card Kelurahan Cibangkong diklik
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => CibangkongPage()),
                    );
                  },
                  child: Card(
                    child: ListTile(
                      leading: Image.asset(
                        'assets/images/tree.png',
                        width: 24.0,
                        height: 24.0,
                      ),
                      title: Text('Kelurahan Cibangkong (2018 - 2020)'),
                      subtitle: Text('Jumlah Lokasi: 2 lokasi'),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
