import 'package:flutter/material.dart';

class SamojaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Samoja'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0,
            0.0), // Jarak dari pinggir layar (3 cm) dan AppBar (20.0)
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Card(
              color: Color.fromARGB(255, 92, 167, 150),
              child: ListTile(
                title: Text('Kelurahan Samoja'),
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(16.0),
              color: Colors.white,
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          '2018 - 2019\n\n - Taman RW (RT 07 RW 07 ) \n\n 2020\n\n - RTH Fasilitas Gardu PLN (Jl. Laswi RW 04)\n\n - RTH Bermain SMPN 4 (Jl. Samoja RW 09)\n\n - RTH Trotoar Taman PU (Jl. Pacar RW 10)\n\n',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
