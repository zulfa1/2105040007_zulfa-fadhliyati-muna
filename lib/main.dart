import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:grosss/login.dart';
import 'package:grosss/home.dart';
import 'package:grosss/register.dart';
import 'package:grosss/welcome.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Define your custom color
  final Color myPrimaryColor = Color.fromARGB(255, 92, 167, 150);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: MaterialColor(
          myPrimaryColor.value,
          <int, Color>{
            50: Color.fromARGB(255, 92, 167, 150),
            100: Color.fromARGB(255, 92, 167, 150),
            200: Color.fromARGB(255, 92, 167, 150),
            300: Color.fromARGB(255, 92, 167, 150),
            400: Color.fromARGB(255, 92, 167, 150),
            500: Color.fromARGB(255, 92, 167, 150),
            600: Color.fromARGB(255, 92, 167, 150),
            700: Color.fromARGB(255, 92, 167, 150),
            800: Color.fromARGB(255, 92, 167, 150),
            900: Color.fromARGB(255, 92, 167, 150),
          },
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/welcome': (context) => WelcomePage(),
        '/login': (context) => LoginPage(),
        '/home': (context) => HomePage(),
        '/register': (context) => RegisterPage(),
      },
      home: WelcomePage(),
    );
  }
}
