import 'package:flutter/material.dart';

class BinongPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Binong'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0,
            0.0), // Jarak dari pinggir layar (3 cm) dan AppBar (20.0)
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Card(
              color: Color.fromARGB(255, 92, 167, 150),
              child: ListTile(
                title: Text('Kelurahan Binong'),
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(16.0),
              color: Colors.white,
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          '2018 - 2019\n\n - Taman RW (RW 01 Jl. Gatot Subroto)\n\n - Taman RW (RW 07 Jl. Binongjati )\n\n - PTaman RW (RW 10)\n\n 2020\n\n - Sawah/ Kebun Kangkung (Jl. Binong Jati RW 05)\n\n - Sawah /Kebun Kagkung (Jl. Binong Jati Ii RT 04 RW 06)\n\n - Sawah /Kebun Kagkung (Jl. Binong Jati Ii RT  RW 6)\n\n - Bekas Situ Otong Rumput (Jl. Binong Jati Ii RT 04 RW 06)\n\n - Kebun Produktif - Tanaman Pelindung - Tanaman Obat   - Dll (Jl. H.Basuki RT 07 RW 10)\n\n - Kampung Berkebun /Halam Berkebun  (Jl. H.Basuki RT 03 RW 10)',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
