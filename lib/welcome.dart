import 'package:flutter/material.dart';
import 'login.dart';

class WelcomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => LoginPage()),
          );
        },
        child: Container(
          color: Color.fromARGB(255, 211, 230, 223),
          padding: const EdgeInsets.all(110.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/save.png',
                width: 170.0,
                height: 170.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
