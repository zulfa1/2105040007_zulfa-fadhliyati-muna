import 'package:flutter/material.dart';

class GumuruhPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gumuruh'),
      ),
      backgroundColor: Color.fromARGB(255, 217, 247, 236),
      body: Padding(
        padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0,
            0.0), // Jarak dari pinggir layar (3 cm) dan AppBar (20.0)
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Card(
              color: Color.fromARGB(255, 92, 167, 150),
              child: ListTile(
                title: Text('Kelurahan Gumuruh'),
              ),
            ),
            SizedBox(height: 20.0),
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(16.0),
              color: Colors.white,
              child: RichText(
                text: TextSpan(
                  style: TextStyle(
                    fontSize: 16.0,
                    color: Colors.black,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                          '2018 - 2019\n\n - Taman Herbal Hejo (RT 09 RW 06) \n\n - Taman Siwal (RT 07 RW 05)\n\n - Pemakaman (TPU Gumuruh)\n\n 2020\n\n - Taman Bunga Dan Pohon-Pohon (Jl. Gumuruh RW 05)\n\n - Pemakaman - Pohon-Pohon Dan Rumput (Jl. Gumuruh RW 06)\n\n - Bunga - Rumput Dan Pohon (Jl. Rancamanyar RW 08)\n\n - Bunga - Rumput Dan Pohon (Jl. Libra RW 09)\n\n - Bunga - Pohon - Tanaman Pelindung - Tanaman Obat  - Dll (Jl. Salendro Timur RW 10)\n\n - Kebun-Kebun - Pohon Dan Rumput  (Jl. Jati Mulya Dan Bbk. Jati RW 11)',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
